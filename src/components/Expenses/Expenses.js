import { useState } from 'react'

import './Expenses.css'
import Card from '../UI/Card'
import ExpensesFilter from './ExpensesInternal/ExpensesFilter/ExpensesFilter'
import ExpensesList from './ExpensesInternal/ExpensesList'
import ExpensesChart from './ExpensesInternal/ExpensesChart/ExpensesChart'

function Expenses({ items }) {
  const [expenseYearFilter, setExpenseYearFilter] = useState('2021')

  const expenseFilterSelectedHandler = (expenseYearFilterSelected) => {
    // setup expenseFilter emitted from child to state in Expenses
    setExpenseYearFilter(expenseYearFilterSelected)
  }

  const expensesFiltered = items.filter(
    (item) => expenseYearFilter === item.date.getFullYear().toString(),
  )

  return (
    <Card className="expenses">
      <ExpensesFilter
        selectedFilter={expenseYearFilter}
        onExpenseFilterSelected={expenseFilterSelectedHandler}
      />
      <ExpensesChart items={expensesFiltered} />
      <ExpensesList
        items={expensesFiltered}
      />
    </Card>
  )
}

export default Expenses
