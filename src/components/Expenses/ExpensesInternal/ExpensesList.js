import ExpenseItem from './ExpenseItem'

import './ExpensesList.css'

const ExpensesList = ({ items }) => {
  if (items.length === 0) {
    return <p className="white-text"> No expenses items found </p>
  }
  return (
    <ul className="expenses-list">
      {items.map((item, i) => {
        return (
          <ExpenseItem
            key={item.id}
            date={item.date}
            title={item.title}
            amount={item.amount}
          />
        )
      })}
    </ul>
  )
}

export default ExpensesList
