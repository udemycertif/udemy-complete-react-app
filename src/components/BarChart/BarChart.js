import BarElmt from './BarChartInternal/BarElmt'
import './BarChart.css'

const BarChart = ({ dataPoints }) => {
  const values = dataPoints.map((p) => p.value)
  const max = Math.max(...values)

  return (
    <div className="chart">
      {dataPoints.map((p) => {
        return (
          <BarElmt
            key={p.label}
            elmtLabel={p.label}
            elmtData={p.value}
            maxValue={max}
          />
        )
      })}
    </div>
  )
}

export default BarChart
