import './BarElmt.css'

const BarElmt = ({ elmtLabel, elmtData, maxValue }) => {
  let fillHeight = '0%'

  if (maxValue > 0) {
    fillHeight = Math.round((elmtData / maxValue) * 100) + '%'
  }

  return (
    <div className="chart-bar">
      <div className="chart-bar__inner">
        <div className="chart-bar__fill" style={{ height: fillHeight }}></div>
      </div>
      <div className="chart-bar__label">{elmtLabel}</div>
    </div>
  )
}

export default BarElmt
