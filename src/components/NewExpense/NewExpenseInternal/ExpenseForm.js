import { useState } from 'react'
import './ExpenseForm.css'

const ExpenseForm = ({onSaveExpenseData, onCancelFormSubmit}) => {
    /** Multiple useState way */
    const [enteredTitle, setEnteredTitle] = useState('');
    const [enteredAmount, setEnteredAmount] = useState('');
    const [enteredDate, setEnteredDate] = useState('');

    /** One useState way */
    // const [userInput, setUserInput] = useState({
    //     enteredTitle: '',
    //     enteredAmount: '',
    //     enteredDate: '',
    // })

    const titleChangeHandler = (event) => {
        /**
         * Better way of setup state when using one useState way, because here react assure that previous state is 
         * actually the previous state values
         */
        // setUserInput((prevState) => {
        //     return {...prevState, enteredTitle: event.target.value}
        // })
        /**
         * Bad practice way because setState is async, so we can't be sure of the value of userInput 
         * when setState is applied
         */ 
        // 
        // setUserInput({
        //     ...userInput,
        //     enteredTitle: event.target.value
        // });
        
        setEnteredTitle(event.target.value);
    }

    const amountChangeHandler = (event) => {
        // setUserInput((prevState) => {
        //     return {...prevState, enteredAmount: event.target.value}
        // })
        // setUserInput({
        //     ...userInput,
        //     enteredAmount: event.target.value
        // });

        setEnteredAmount(event.target.value);
    }

    const dateChangeHandler = (event) => {
        // setUserInput((prevState) => {
        //     return {...prevState, enteredDate: event.target.value}
        // })
        // setUserInput({
        //     ...userInput,
        //     enteredDate: event.target.value
        // });

        setEnteredDate(event.target.value);
    }

    const resetForm = () => {
        setEnteredTitle('');
        setEnteredAmount('');
        setEnteredDate('');
    }

    const submitExpenseFormHandler = (event) => {
        event.preventDefault();

        const expenseData = {
            title: enteredTitle,
            amount: +enteredAmount,
            date: new Date(enteredDate),
        } 

        onSaveExpenseData(expenseData);
        resetForm();
    }

    return (
        <form onSubmit={submitExpenseFormHandler}>
            <div className="new-expense__controls">
                <div className="new-expense__control">
                    <label>Title</label>
                    <input type="text" value={enteredTitle} onChange={titleChangeHandler} />
                </div>
                <div className="new-expense__control">
                    <label>Amount</label>
                    <input type="number" min="0.01" step="0.01" value={enteredAmount} onChange={amountChangeHandler} />
                </div>
                <div className="new-expense__control">
                    <label>Date</label>
                    <input type="date" min="2019-01-01" step="2022-12-31" value={enteredDate} onChange={dateChangeHandler} />
                </div>
            </div>
            <div className="new-expense__actions">
                <button onClick={onCancelFormSubmit}> Cancel </button>
                <button type="submit"> Add Expense </button>
            </div>
        </form>
    )
}

export default ExpenseForm;