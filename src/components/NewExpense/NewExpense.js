import {useState} from 'react';

import ExpenseForm from "./NewExpenseInternal/ExpenseForm";
import './NewExpense.css'

const NewExpense = ({onAddExpenseItem}) => {
    const [isFormDisplayed, setIsFormDisplayed] = useState(false);

    const cancelFormSubmitHandler = () => {
        setIsFormDisplayed(false);
    }

    const saveExpenseDataHandler = (expenseDataEmitted) => {
        const expenseData = {
            ...expenseDataEmitted,
            id: Math.random().toString()
        }

        onAddExpenseItem(expenseData);
        setIsFormDisplayed(false);
    }

    const addNewExpenseHandler = () => {
        setIsFormDisplayed(true);
    } 

    const setupNewExpenseContent = () => {
        if(!isFormDisplayed) {
            return (
                <button onClick={addNewExpenseHandler}>Add new Expense</button>
            )
        }

        return (
            <ExpenseForm onSaveExpenseData={saveExpenseDataHandler} onCancelFormSubmit={cancelFormSubmitHandler} />
        )
    } 

    const newExpenseContent = setupNewExpenseContent();

    return (
        <div className="new-expense">
            {newExpenseContent}
        </div>
    )
}

export default NewExpense;