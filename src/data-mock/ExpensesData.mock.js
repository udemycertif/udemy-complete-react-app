export const expensesDataMock = [
  {
    id: '0',
    title: 'Yolo',
    amount: 666.66,
    date: new Date(2020, 7, 14),
  },
  { id: '1', title: 'Easy TV', amount: 4.99, date: new Date(2021, 2, 12) },
  {
    id: '2',
    title: 'Hey hey',
    amount: 182.22,
    date: new Date(2021, 2, 28),
  },
  {
    id: '3',
    title: 'Yowa',
    amount: 100,
    date: new Date(2021, 5, 12),
  },
]
