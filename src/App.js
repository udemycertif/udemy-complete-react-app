import { useState } from 'react'
import './App.css'
import Expenses from './components/Expenses/Expenses'
import NewExpense from './components/NewExpense/NewExpense'
import { expensesDataMock } from './data-mock/ExpensesData.mock'

function App() {
  const [expensesData, setExpensesData] = useState(expensesDataMock)

  const addExpenseItemHandler = (newExpenseItem) => {
    setExpensesData((previousExpensesData) => {
      return [newExpenseItem, ...previousExpensesData];
    })
  }

  return (
    <div className="App">
      <p>My First React app</p>
      <NewExpense onAddExpenseItem={addExpenseItemHandler} />
      <Expenses items={expensesData} />
    </div>
  )
}

export default App
